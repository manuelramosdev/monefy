import csv

class Move:
  def __init__(self, date, account, category, description, amount, isTransfer):
    self.date = date
    self.account = account
    self.category = category
    self.description = description
    self.amount = amount
    self.isTransfer = isTransfer

def createMoveFromRow(row):
	indexDate = 0
	indexAccount = 1
	indexCategory = 2
	indexDescription = 7
	indexAmount = 3
	date = row[indexDate]
	account = row[indexAccount]
	category = row[indexCategory]
	description = row[indexDescription] if len(row) is indexDescription + 1 else ""
	amount = row[indexAmount]
	isTransfer = category[0:2] == 'To'
	return Move(date, account, category, description, amount, isTransfer)

def importCSV(filename):
	with open(filename, 'rb') as f:
		reader = csv.reader(f, delimiter=';')
		return [createMoveFromRow(row) for row in reader]
